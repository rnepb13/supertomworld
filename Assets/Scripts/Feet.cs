﻿using UnityEngine;
using System.Collections;

public class Feet : MonoBehaviour {

	private Hero player;

	void Start() {
		player = this.transform.parent.gameObject.GetComponent<Hero> ();
	}

	// let main controller know when events occur at player feet
	void OnTriggerEnter(Collider other) {
		player.OnFeetHitGround (other);
	}

	void OnTriggerExit(Collider other) {
		player.OnFeetLeftGround (other);
	}
}
